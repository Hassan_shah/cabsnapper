package app.cabsnapper.utils;

import com.mukesh.countrypicker.CountryPicker;

/**
 * Created by saadnayyer on 8/31/2017.
 */

public class CountryPickerDialog extends CountryPicker {
    CountryPicker picker;
    OnDestroyListener listener;



    public CountryPickerDialog(String title, OnDestroyListener listener){
        this.picker = super.newInstance(title);
        this.listener = listener;
    }

    public CountryPickerDialog() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener.onDestroy();
    }

    public interface OnDestroyListener{
        void onDestroy();
    }
}
