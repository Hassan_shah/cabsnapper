package app.cabsnapper.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by Mirza on 24-Jan-18.
 */

public  class UIHelper {


    public static void showSnackBar(Context ctx, View v, String text, int color){
        if (v != null)
            Snackbar.make(v, text, Snackbar.LENGTH_SHORT)
                    .setActionTextColor(color)
                    .setDuration(4000)
                    .show();
    }
}
